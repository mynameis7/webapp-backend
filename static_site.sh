#!/bin/bash
if [ $1 == "startdev" ]; then
    docker-compose -f webserver.yml -f minio.yaml up;
elif [ $1 == "start" ]; then
    docker-compose -f webserver.yml -f minio.yaml up -d;
elif [ $1 == "stop" ]; then
    docker-compose -f webserver.yml -f minio.yaml down;
fi