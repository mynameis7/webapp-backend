# Backend Web Stack

This docker compose sets up the following:
Hasura Graph-QL 
Minio S3 Bucket System
FusionAuth auth system (for jwt user roles in hasura)


For the FusionAuth system you will need to ensure that the following is run on the docker host:

```
sudo sysctl -w vm.max_map_count=262144

```

